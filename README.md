# homeapp-v2

Project created with the purpose of trying different approaches (not necessarely good ones) for the architecture of a MERN aplication. It's also a useful app for your home.

## Building the app

In the API project, we use the native C++ version of bcrypt. Because of this you need to have a C++ build environment installed. I recommend installing the Visual Studio Build Tools, as instructed at the [node-gyp repository](https://github.com/nodejs/node-gyp#on-windows). Alternatively, you can swap the bcrypt dependency for the bcryptjs module from npm.

## Running the app

To run, create a .env file in /api and define the variables:
```
MONGO_USER
MONGO_PASSWORD
MONGO_SERVER_URL
JWT_SECRET
```

Based on the MVC pattern, the home App features user authentication, shopping lists, a notice board, and (TODO™) recipes. The shopping lists are a neat way to organize different things you need to buy. Items are sorted and you can cross them out if you wish. Once you write an item down, it will be stored so you dont need to type it all out on any other list you make.

The CI/CD pipeline was developed for Heroku, but can be easily changed to support other cloud-platform-as-a-service providers.

![example image 1](readme_assets/homeapp-v2-example1.jpg)
![example image 2](readme_assets/homeapp-v2-example2.jpg)
