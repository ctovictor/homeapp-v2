import ShoppingList from '../models/ShoppingListModel'
import { getDate_ddmmyyyy } from '../utils/getDate_ddmmyyyy'

export const createShoppingList = (req, res) => {
    const name = `Lista do dia ${getDate_ddmmyyyy()}`

    let shoppingList = new ShoppingList({
        name,
        items: [],
    })

    shoppingList
        .save()
        .then((doc) => {
            console.log(doc)
            res.send(doc)
        })
        .catch((err) => {
            console.error(err)
            res.send(err)
        })
}

export const getAllShoppingLists = (req, res) => {
    console.log('all shopping lists')
    ShoppingList.find()
        .then((doc) => {
            res.send(doc)
        })
        .catch((err) => {
            console.error(err)
        })
}

export const updateShoppingList = (req, res) => {
    const updateOptions = { runValidators: true }
    ShoppingList.findByIdAndUpdate(
        req.params.id,
        { ...req.body },
        updateOptions,
        (err, doc) => {
            if (err) {
                console.error(err)
                res.send(err.message)
            }
            res.send('updated successfully')
        }
    )
}

export const deleteShoppingList = (req, res) => {
    console.log(`${req.username} asked to delete a shoppingList`)
    ShoppingList.findByIdAndRemove(req.params.id, (err) => {
        if (err) return next(err)
        res.send('deleted successfully')
    })
}
