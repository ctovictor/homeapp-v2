import ShoppingListItem from '../models/ShoppingListItemModel'

export const addShoppingListItem = (req, res) => {
    const { text, done = false } = req.body

    let item = new ShoppingListItem({
        text,
        done,
    })

    item.save()
        .then((doc) => {
            console.log(doc)
            res.send(doc)
        })
        .catch((err) => {
            console.error(err)
        })
}

export const getAllShoppingListItems = (req, res) => {
    const sortQuery = req.query.sort.split(',')
    const sortArray = []
    for (let i = 0; i < sortQuery.length; i++) {
        if (i % 2) sortArray.push([sortQuery[i - 1], sortQuery[i]])
    }

    ShoppingListItem.find()
        .sort(sortArray)
        .then((doc) => {
            //console.log(doc);
            res.send(doc)
        })
        .catch((err) => {
            console.error(err)
        })
}
export const updateShoppingListItem = (req, res, next) => {
    ShoppingListItem.findByIdAndUpdate(
        req.params.id,
        { ...req.body },
        (err) => {
            if (err) return next(err)
            res.send()
        }
    )
}

export const deleteShoppingListItem = (req, res) => {
    console.log('someone asked to delete a shoppingListItem')
    ShoppingListItem.findByIdAndRemove(req.params.id, (err) => {
        if (err) return next(err)
        res.send('deleted successfully')
    })
}
