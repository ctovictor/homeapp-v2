import Notice from '../models/NoticeModel'
import { getDate_ddmmyyyy } from '../utils/getDate_ddmmyyyy'

//Simple version, without validation or sanitation
export const addNotice = (req, res) => {
    const today = getDate_ddmmyyyy()

    const { text, done } = req.body
    let notice = new Notice({
        text,
        date: today,
        done,
    })

    notice
        .save()
        .then((doc) => {
            console.log(doc)
            res.send('notice Created successfully')
        })
        .catch((err) => {
            console.error(err)
            res.status(422).send({
                erro: err.message,
            })
            res
        })
}

export const getAllNotices = (req, res) => {
    console.log('someone asked to see all notices')
    Notice.find()
        .then((doc) => {
            console.log(doc)
            res.send(doc)
        })
        .catch((err) => {
            console.error(err)
        })
}

export const updateNotice = (req, res, next) => {
    console.log('updating notice', req.body)
    Notice.findByIdAndUpdate(req.params.id, { ...req.body }, (err) => {
        if (err) return next(err)
        res.send()
    })
}

export const deleteNotice = (req, res, next) => {
    console.log('someone asked to delete a notice')
    Notice.findByIdAndRemove(req.params.id, (err) => {
        if (err) return next(err)
        res.send('deleted successfully')
    })
}
