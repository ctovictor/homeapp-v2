import jwt from 'jsonwebtoken'

import User from '../models/UserModel'
import bcrypt from 'bcrypt'

//Simple version, without validation or sanitation
export const registerUser = (req, res) => {
    const { username, password } = req.body
    if (!username || !password) {
        res.status(422).send({ error: 'missing username or password' })
        return
    }
    const user = new User({ username, password })
    user.save(err => {
        if (err) {
            console.log(err)
            res.status(500).send('Error registering new user please try again.')
        } else {
            res.status(200).send('User created successfully')
        }
    })
}

export const getUsers = (req, res) => {
    User.find()
        .then(docs => {
            res.send(docs)
        })
        .catch(err => {
            console.error(err)
        })
}

export const authenticateUser = (req, res) => {
    const { password, username } = req.body
    console.log('someone trying to log in with username \'', username, '\'')
    User.findOne({ username }, (err, user) => {
        if (err) {
            console.error(err)
            res.status(500).json({
                error: 'Internal server error, could not search for user',
            })
        } else if (!user) {
            res.status(401).json({
                error: 'Incorrect username or password',
            })
        } else {
            bcrypt.compare(password, user.password, (err, bcryptRes) => {
                if (err) {
                    res.status(500).send({ error: "erro desconhecido" })
                } else if (bcryptRes) {
                    const payload = { username }
                    const token = jwt.sign(payload, process.env.JWT_SECRET, {
                        expiresIn: '14d',
                    })
                    res.cookie('token', token, {}).sendStatus(200)
                }
                else {
                    res.status(401).send({ error: "Incorrect username or password" })
                }
            })

        }
    })
}
