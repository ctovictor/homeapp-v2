const express = require('express')
const router = express.Router()
import withAuth from '../middlewares/withAuth'
import { notice, recipe, shoppingListItem, shoppingList, user } from './'

//endpoints
router.use('/shopping-list-items', withAuth, shoppingListItem)
router.use('/shopping-lists', withAuth, shoppingList)
router.use('/notices', withAuth, notice)
router.use('/recipes', withAuth, recipe)
router.use('/users', user)

export const apiRoutes = router
