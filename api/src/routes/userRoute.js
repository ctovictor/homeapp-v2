import express from 'express'
import {
    registerUser,
    getUsers,
    authenticateUser,
} from '../controllers/userController'
const router = express.Router()

//router.get('/', getUsers)

// router.post('/register', registerUser)

router.post('/login', authenticateUser)

export const user = router
