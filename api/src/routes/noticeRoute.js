import express from 'express'
const router = express.Router()
import {
    addNotice,
    getAllNotices,
    deleteNotice,
    updateNotice,
} from '../controllers/noticeController'

router.get('/', getAllNotices)

//router.get('/byprice/:price', product_controller.product_by_price);

router.post('/', addNotice)

// router.get('/:id', todoTask_controller.get_task_by_id);

router.put('/:id', updateNotice)

router.delete('/:id', deleteNotice)

export const notice = router
