import express from 'express'
import {
    getAllShoppingLists,
    createShoppingList,
    updateShoppingList,
    deleteShoppingList,
} from '../controllers/shoppingListController'

const router = express.Router()

router.get('/', getAllShoppingLists)

//router.get('/byprice/:price', product_controller.product_by_price);

router.post('/', createShoppingList)

// router.get('/:id', shoppingListItem_controller.get_task_by_id);

router.patch('/:id', updateShoppingList)

router.delete('/:id', deleteShoppingList)

export const shoppingList = router
