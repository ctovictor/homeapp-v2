import express from 'express'
const router = express.Router()
import {
    addShoppingListItem,
    getAllShoppingListItems,
    deleteShoppingListItem,
    updateShoppingListItem
} from '../controllers/shoppingListItemController'

router.get('/', getAllShoppingListItems)

//router.get('/byprice/:price', product_controller.product_by_price);

router.post('/', addShoppingListItem)

// router.get('/:id', shoppingListItem_controller.get_task_by_id);

router.put('/:id', updateShoppingListItem);

router.delete('/:id', deleteShoppingListItem)

export const shoppingListItem = router
