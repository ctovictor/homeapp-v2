import path from 'path'

export const getRootPath = (currentServerDir) => {
    const currentFolderName = path.basename(path.resolve(currentServerDir))
    if (currentFolderName === 'build') {
        return path.join(currentServerDir, '..','..')
    } else return path.join(currentServerDir, '..')
}
