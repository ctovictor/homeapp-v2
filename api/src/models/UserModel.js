import mongoose from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcrypt'

const SALT_ROUNDS = 10

let UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        timestamps: true,
        maxlength: 10,
        validate: value => {
            return !validator.isEmpty(value)
        },
    },
    password: {
        type: String,
        required: true,
    },
})

//schema methods

//hooks
UserSchema.pre('save', function(next) {
    //TODO checar se isNew e isModified existe e funciona
    if (this.isNew || this.isModified('password')) {
        bcrypt.hash(this.password, SALT_ROUNDS, (err, hashedPassword) => {
            if (err) {
                next(err)
            } else {
                this.password = hashedPassword
                next()
            }
        })
    } else {
        next()
    }
})

export default mongoose.model('User', UserSchema)
