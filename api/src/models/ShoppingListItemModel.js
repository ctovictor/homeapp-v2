import mongoose from 'mongoose'
import validator from 'validator'

export const ShoppingListItemSchema = new mongoose.Schema(
    {
        text: {
            type: String,
            required: true,
            unique: false,
            lowercase: false,
            timestamps: true,
            max: 1000,
            validate: value => {
                return !validator.isEmpty(value)
            },
        },
        done: {
            type: Boolean,
            required: true,
        },

    },
    { timestamps: true }
)

export default mongoose.model('ShoppingListItem', ShoppingListItemSchema)
