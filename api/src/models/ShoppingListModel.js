import mongoose from 'mongoose'
import validator from 'validator'
import { ShoppingListItemSchema } from './ShoppingListItemModel'

let ShoppingListSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            unique: false,
            lowercase: false,
            max: 1000,
            validate: (value) => {
                return !validator.isEmpty(value)
            },
        },
        items: [
            {
                type: ShoppingListItemSchema,
                required: true,
            },
        ],
    },
    { timestamps: true }
)

export default mongoose.model('ShoppingList', ShoppingListSchema)
