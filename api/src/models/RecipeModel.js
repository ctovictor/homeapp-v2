import mongoose from 'mongoose'
import validator from 'validator'

let RecipeSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: false,
        lowercase: false,
        timestamps: true,
        max: 1000,
        validate: value => {
            return !validator.isEmpty(value)
        },
    },

    ingredientsList: {
        // type: [{type:String}],
        type: [String],
        required: true,
        unique: false,
        lowercase: false,
        timestamps: true,
        max: 1000,
    },

    method: {
        type: String,
        required: true,
        unique: false,
        lowercase: false,
        timestamps: true,
        max: 1000,
        validate: value => {
            return !validator.isEmpty(value)
        },
    },
})

export default mongoose.model('Recipe', RecipeSchema)
