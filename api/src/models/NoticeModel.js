import mongoose from 'mongoose'
import validator from 'validator'

let NoticeSchema = new mongoose.Schema(
    {
        text: {
            type: String,
            required: true,
            unique: false,
            lowercase: false,
            timestamps: true,
            max: 1000,
            validate: value => {
                return !validator.isEmpty(value)
            },
        },
        date: {
            type: String,
            required: true,
        },
        done: {
            type: Boolean,
            required: true,
        },
    },
    { timestamps: true }
)

export default mongoose.model('Notice', NoticeSchema)
