import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import express from 'express'
import mongoose from 'mongoose'
import { apiRoutes } from './src/routes'
import path from 'path'
import { getRootPath } from './src/utils/getRootPath'
require('dotenv').config()

const PORT = process.env.PORT || 8080
const SECRET = process.env.SECRET

const app = express()

//TODO ver melhor jeito de inicializar e organizar a conexao com o banco
// const mongoURI = 'mongodb://localhost/homeApp-v2'
const mongoURI = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_SERVER_URL}`

mongoose.connect(mongoURI, { useNewUrlParser: true }, function (err) {
    if (err) {
        throw err
    } else {
        console.log(`Successfully connected to database`)
    }
})

//middlewares
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieParser())

//endpoints
app.use('/api', apiRoutes)

//web app
const rootPath = getRootPath(__dirname)
const webAppPath = path.join(rootPath, 'web', 'build')
const webApp = path.join(webAppPath, 'index.html')
app.use(express.static(webAppPath))
app.get('/*', function (req, res) {
    res.sendFile(webApp)
})

app.listen(PORT, () => console.log(`Listening on port ${PORT}`))
