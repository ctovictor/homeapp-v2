import React from 'react'
import AuthContext from 'src/context/AuthContext'

export const withAuthContext = Component => props => (
    <AuthContext.Consumer>
        {state => <Component {...props} context={state}></Component>}
    </AuthContext.Consumer>
)

export default withAuthContext
