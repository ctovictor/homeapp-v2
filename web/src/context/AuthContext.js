import React from 'react'

const AuthContext = React.createContext(null)
AuthContext.displayName='auth-context'

export default AuthContext
