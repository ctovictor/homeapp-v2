import React from 'react'

const LoginContext = React.createContext(null)
LoginContext.displayName='login-context'

export default LoginContext
