import React from 'react'
import AuthProvider from './AuthProvider'

class AppProvider extends React.Component {
    render() {
        return <AuthProvider>{this.props.children}</AuthProvider>
    }
}

export default AppProvider
