import React from 'react'
import AuthContext from 'src/context/AuthContext'
import { loginUser } from 'src/requests'
import Cookies from 'js-cookie'

class AuthProvider extends React.Component {
    state = {
        authRes: null,
        token: null,
        loading: true,
        dateLogged: '',
    }
    componentDidMount() {
        const token = Cookies.get('token') || null
        const dateLogged = Cookies.get('dateLogged') || 0
        this.setState({ token, loading: false, dateLogged })
    }

    login = async (username, password) => {
        let success = false
        try {
            const res = await loginUser({ username, password })
            const dateLogged = new Date()
            this.setState({
                authRes: res,
                token: Cookies.get('token'),
                dateLogged,
            })
            Cookies.set('dateLogged', dateLogged)
            success = true
        } catch (e) {
            console.warn(e)
        }
        return success
    }

    isAuthenticated = () => {
        return !!this.state.token && this.tokenStillValid();
    }

    tokenStillValid = () => {
        let stillValid = false
        if (!!this.state.dateLogged) {
            const diffTime = Math.abs(new Date() - new Date(this.state.dateLogged))
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))
            stillValid = diffDays < 14
        }
        return stillValid
    }

    logOut = () => {
        this.setState({ token: null, dateLogged: null })
    }

    render() {
        if (this.state.loading) return <div>Aguarde</div>
        return (
            <AuthContext.Provider
                value={{
                    authState: this.state,
                    authActions: {
                        login: this.login,
                        logOut: this.logOut,
                        isAuthenticated: this.isAuthenticated
                    },
                }}
            >
                {this.props.children}
            </AuthContext.Provider>
        )
    }
}

export default AuthProvider
