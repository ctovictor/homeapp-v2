export const getDate_ddmmyyyy = (date) => {
    let today = date ? new Date(date) : new Date()
    let dd = today.getDate()
    let mm = today.getMonth() + 1 //January is 0!
    let yyyy = today.getFullYear()
    today = dd + '/' + mm + '/' + yyyy
    return today
}
