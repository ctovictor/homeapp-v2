const axios = require('axios')

const base_api_url = '/api'

export const encodeQueryData = (data) => {
    let ret = []
    for (let d in data)
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]))
    return ret.join('&')
}

export const getAll = (entity) => {
    console.log('requested all ' + entity)
    return axios.get(`${base_api_url}/${entity}/all/`)
}

//notices
export const getAllNotices = (entity) => {
    return axios.get(`${base_api_url}/notices/`)
}
export const postNotice = (body) => {
    return axios.post(`${base_api_url}/notices/`, body)
}
export const modifyOneNotice = (id, body) => {
    return axios.put(`${base_api_url}/notices/${id}`, body)
}
export const deleteNotice = (id) => {
    return axios.delete(`${base_api_url}/notices/` + id)
}

//shopping list items
export const getAllShoppingListItems = ({ sortParams }) => {
    let sortQuery = ''
    Object.keys(sortParams).forEach((key) => {
        sortQuery += `${key},${sortParams[key]},`
    })
    return axios.get(`${base_api_url}/shopping-list-items?sort=${sortQuery}`)
}
export const postShoppingListItem = (body) => {
    return axios.post(`${base_api_url}/shopping-list-items/`, body)
}
export const modifyOneShoppingListItem = (id, body) => {
    return axios.put(`${base_api_url}/shopping-list-items/${id}`, body)
}
export const deleteShoppingListItem = (id) => {
    return axios.delete(`${base_api_url}/shopping-list-items/` + id)
}

//shopping lists
export const getAllShoppingLists = () => {
    let sortQuery = ''
    // Object.keys(sortParams).forEach(key => {
    //     sortQuery += `${key},${sortParams[key]},`
    // })
    return axios.get(`${base_api_url}/shopping-lists`)
}
export const createShoppingList = () => {
    return axios.post(`${base_api_url}/shopping-lists`)
}
export const updateShoppingList = ({ id, newValue }) => {
    return axios.patch(`${base_api_url}/shopping-lists/${id}`, newValue)
}
export const deleteShoppingList = (id) => {
    return axios.delete(`${base_api_url}/shopping-lists/${id}`)
}

//recipes
export const deleteRecipe = (id) => {
    return axios.delete(`${base_api_url}/recipes/` + id)
}
export const postRecipe = (recipe) => {
    return axios.post(`${base_api_url}/recipes/`, recipe)
}
export const editRecipe = (recipe) => {
    return axios.put(`${base_api_url}/recipes/`, recipe)
}

//user
export const loginUser = ({ username, password }) => {
    return axios.post(`${base_api_url}/users/login`, {
        username,
        password,
    })
}

// exports.postOne = (entity, body) => {
//     return axios.post(`${base_api_url}/${entity}`, body, {
//         validateStatus: status => status < 500,
//     })
// }
