import React from 'react'

export const ShoppingListContext = React.createContext()
ShoppingListContext.displayName = 'ShoppingListContext'
export const withShoppingListContext = (Component) => (props) => (
    <ShoppingListContext.Consumer>
        {(state) => <Component {...props} shoppingListContext={state}></Component>}
    </ShoppingListContext.Consumer>
)

export const ShoppingListControlContext = React.createContext()
ShoppingListControlContext.displayName = 'ShoppingListControlContext'
export const withShoppingListControlContext = (Component) => (props) => (
    <ShoppingListControlContext.Consumer>
        {(state) => <Component {...props} shoppingListControl={state}></Component>}
    </ShoppingListControlContext.Consumer>
)
