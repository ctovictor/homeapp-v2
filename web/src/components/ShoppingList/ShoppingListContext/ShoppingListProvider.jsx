import React from 'react'
import {
    ShoppingListContext,
    ShoppingListControlContext,
} from './ShoppingListContext'
import {
    getAllShoppingLists,
    updateShoppingList,
    deleteShoppingList,
} from 'src/requests'

class ShoppingListProvider extends React.Component {
    state = {
        currentShoppingListId: '',
        shoppingLists: [],
        loading: true,
    }
    constructor(props) {
        super(props)
        this.shoppingListControl = {
            getShoppingList: this.getShoppingList,
            addShoppingList: this.addShoppingList,
            addItemToShoppingList: this.addItemToShoppingList,
            handleDeleteShoppingList: this.handleDeleteShoppingList,
            handleToggleDoneOnItem: this.handleToggleDoneOnItem,
            handleRemoveItemFromShoppingList: this
                .handleRemoveItemFromShoppingList,
            handleChangeShoppingListName: this.handleChangeShoppingListName,
        }
    }
    async componentDidMount() {
        const shoppingLists = await this.fetchFromDatabase()
        this.setState({ shoppingLists, loading: false })
    }
    fetchFromDatabase = async () => {
        try {
            const res = await getAllShoppingLists()
            const shoppingLists = res.data
            return shoppingLists
        } catch (e) {
            console.error(e)
        }
    }
    handleRemoveItemFromShoppingList = async (id, itemId) => {
        const shoppingList = this.getShoppingList(id)
        const newShoppingList = {
            ...shoppingList,
            items: shoppingList.items.filter((item) => item._id !== itemId),
        }
        await this.updateShoppingList(id, newShoppingList)
    }

    handleToggleDoneOnItem = async (id, itemId) => {
        const shoppingList = this.getShoppingList(id)
        const newShoppingList = {
            ...shoppingList,
            items: shoppingList.items.map((item) =>
                item._id === itemId ? { ...item, done: !item.done } : item
            ),
        }
        await this.updateShoppingList(id, newShoppingList)
    }

    handleDeleteShoppingList = async (id) => {
        await deleteShoppingList(id)
        this.setState({
            shoppingLists: this.state.shoppingLists.filter(
                (shoppingList) => shoppingList._id !== id
            ),
        })
    }
    handleChangeShoppingListName = async (id, newName) => {
        const shoppingList = this.getShoppingList(id)
        await this.updateShoppingList(id, { ...shoppingList, name: newName })
    }

    addItemToShoppingList = (id, item) => {
        const shoppingList = this.getShoppingList(id)
        const newShoppingList = {
            ...shoppingList,
            items: [...shoppingList.items, item],
        }
        this.updateShoppingList(id, newShoppingList)
    }

    updateShoppingList = async (id, newValue) => {
        const updatedShoppingList = await updateShoppingList({ id, newValue })
        const shoppingLists = this.state.shoppingLists.map((shoppingList) => {
            if (shoppingList._id === id) {
                return newValue
            } else {
                return shoppingList
            }
        })
        this.setState({ shoppingLists })
    }

    getShoppingList = (shoppingListId) => {
        const { shoppingLists } = this.state
        return shoppingLists.find((el) => el._id === shoppingListId)
    }
    addShoppingList = (shoppingList) => {
        this.setState({
            shoppingLists: [...this.state.shoppingLists, shoppingList],
        })
    }

    render() {
        return (
            <ShoppingListContext.Provider value={this.state}>
                <ShoppingListControlContext.Provider
                    value={this.shoppingListControl}
                >
                    {this.props.children}
                </ShoppingListControlContext.Provider>
            </ShoppingListContext.Provider>
        )
    }
}

export default ShoppingListProvider
