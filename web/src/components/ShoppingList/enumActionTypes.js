export const enumActionTypes = Object.freeze({
    CLEAR: 'clear', // remove all selected
    CREATE_OPTION: 'create-option', // create new option
    DESELECT_OPTION: 'deselect-option', // deselect option from the list (on multi)
    POP_VALUE: 'pop-value', // remove selected option with backspace (on multi)
    REMOVE_VALUE: 'remove-value', // remove selected option with X button (on multi)
    SELECT_OPTION: 'select-option', // select from list
    SET_VALUE: 'set-value', // ?
})
