import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import { withStyles } from '@material-ui/core/styles'
import DeleteIcon from '@material-ui/icons/Delete'
import React from 'react'
import { Typography } from '@material-ui/core'

class ShoppingListItemsList extends React.Component {
    render() {
        const { classes, items, toggleDoneOnItem, deleteItem } = this.props
        console.log('list', items[0])

        return (
            <List className={classes.root}>
                {items.map((item, index) => (
                    <React.Fragment key={item._id}>
                        <ListItem
                            // dense
                            button
                            className={classes.listItem}
                            onClick={() => toggleDoneOnItem(item._id)}
                        >
                            <ListItemText
                                primary={item.text}
                                className={
                                    item.done
                                        ? classes.strikethroughText
                                        : classes.normalText
                                }
                            />
                            <Typography
                                variant={'body2'}
                                className={classes.fancy}
                            >
                                {item.date}
                            </Typography>
                            {item.done && (
                                <ListItemSecondaryAction>
                                    <IconButton
                                        edge="end"
                                        aria-label="delete"
                                        onClick={() => {
                                            deleteItem(item._id)
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            )}
                        </ListItem>
                    </React.Fragment>
                ))}
            </List>
        )
    }
}

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
    },
    strikethroughText: {
        textDecoration: 'line-through',
    },
    normalText: {
        textDecoration: 'none',
    },
    fancy: {
        marginLeft: '4px',
        fontFamily: 'Norican, cursive',
    },
    listItem: {
        display: 'flex',
        flexDirection: 'row',
    },
})

export default withStyles(styles)(ShoppingListItemsList)
