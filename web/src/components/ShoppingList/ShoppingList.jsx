import { CircularProgress } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import { withStyles } from '@material-ui/core/styles'
import EditIcon from '@material-ui/icons/Edit'
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined'
import { compose } from 'lodash/fp'
import React from 'react'
import { withRouter } from 'react-router'
import CreatableSelect from 'react-select/creatable'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import TextFormModal from 'src/components/TextFormModal'
import {
    withShoppingListItemContext,
    withShoppingListItemControlContext,
} from '../ShoppingListItem/ShoppingListItemContext/ShoppingListItemContext'
import {
    withShoppingListContext,
    withShoppingListControlContext,
} from './ShoppingListContext/ShoppingListContext'
import ShoppingListItemsList from './ShoppingListItemsList'
import { enumActionTypes } from './enumActionTypes'

class ShoppingList extends React.Component {
    currentShoppingListId
    state = {
        selectValue: '',
        editNameModalIsOpen: false,
    }
    constructor(props) {
        super(props)
        const { match } = props
        this.currentShoppingListId = match.params.shoppingListId
    }

    handleToggleDoneOnItem = (itemId) => {
        const {
            shoppingListControl: { handleToggleDoneOnItem },
        } = this.props
        handleToggleDoneOnItem(this.currentShoppingListId, itemId)
    }

    handledeleteItem = async (itemId) => {
        const {
            shoppingListControl: { handleRemoveItemFromShoppingList },
        } = this.props
        handleRemoveItemFromShoppingList(this.currentShoppingListId, itemId)
    }

    handleSelectChange = async (newValue, actionMeta) => {
        if (newValue === null || newValue === undefined) {
            newValue = []
        }

        switch (actionMeta.action) {
            case enumActionTypes.CREATE_OPTION:
                await this.handleCreateOption(newValue)
                break
            case enumActionTypes.SELECT_OPTION:
                this.handleChangeSelectedOption(newValue)
                break
            default:
                break
        }
    }
    handleChangeSelectedOption = (option) => {
        const {
            shoppingListItemControl: { mapOptionToShoppingListItem },
            shoppingListControl: { addItemToShoppingList },
        } = this.props
        addItemToShoppingList(
            this.currentShoppingListId,
            mapOptionToShoppingListItem(option)
        )
    }
    handleCreateOption = async (option) => {
        const {
            shoppingListItemControl: { createShoppingListItem },
            shoppingListControl: { addItemToShoppingList },
        } = this.props
        const newItem = await createShoppingListItem(option.label)
        addItemToShoppingList(this.currentShoppingListId, newItem)
    }

    copyItemsToClipboard = () => {
        const {
            shoppingListControl: { getShoppingList },
        } = this.props
        const shoppingList = getShoppingList(this.currentShoppingListId)
        const el = document.createElement('textarea')
        const items = shoppingList.items.filter((item) => !item.done)
        const text = items.reduce(
            (acumulado, item) => `${acumulado}${item.text}\n`,
            ''
        )
        console.log(text)
        el.value = text
        document.body.appendChild(el)
        el.select()
        document.execCommand('copy')
        document.body.removeChild(el)
        toast('Itens copiados para a área de transferência!', {
            autoClose: 4000,
        })
    }
    handleClickRename = () => {
        this.setState({ editNameModalIsOpen: true })
    }
    handleCloseEditNameModal = () => {
        this.setState({ editNameModalIsOpen: false })
    }
    onConfirmEditName = async (newName) => {
        const {
            shoppingListControl: { handleChangeShoppingListName },
        } = this.props
        await handleChangeShoppingListName(this.currentShoppingListId, newName)
        this.handleCloseEditNameModal()
    }

    render() {
        const {
            classes,
            shoppingListContext: { loading: loadingShoppingList },
            shoppingListControl: { getShoppingList },
            shoppingListItemContext: {
                loading: loadingItems,
                shoppingListItems: allShoppingListItems,
            },
            shoppingListItemControl: { mapShoppingListItemsToOptions },
        } = this.props

        if (loadingItems || loadingShoppingList) {
            return <CircularProgress />
        }

        const shoppingList = getShoppingList(this.currentShoppingListId)
        const shoppingListItemsNotInCurrentShoppingList = allShoppingListItems.filter(
            (item) =>
                !shoppingList.items.some(
                    (alreadySelectedItem) =>
                        item._id === alreadySelectedItem._id
                )
        )
        const options = mapShoppingListItemsToOptions(
            shoppingListItemsNotInCurrentShoppingList
        )
        const sortedItems = [...shoppingList.items].sort((item1, item2) => {
            console.log(`comparando ${item1.text} e ${item2.text}`)
            console.log(`${item1.done} e ${item2.done}`)
            if (item1.done && !item2.done) {
                console.log(`${item2.text} vem antes de ${item1.text}`)
                return 1
            } else if (!item1.done && item2.done) return -1
            else
                return item1.text.localeCompare(item2.text, undefined, {
                    ignorePunctuation: true,
                })
        })
        return (
            <div className={classes.root}>
                <CreatableSelect
                    isClearable
                    onChange={this.handleSelectChange}
                    isLoading={false}
                    value={this.state.selectValue}
                    name="items"
                    options={options}
                    placeholder={'Adicionar items'}
                    loadingMessage={() => 'Carregando'}
                    noOptionsMessage={() => 'Nenhum item encontrado'}
                />
                {loadingShoppingList && <CircularProgress />}
                {!loadingShoppingList && (
                    <React.Fragment>
                        <ShoppingListItemsList
                            items={sortedItems}
                            toggleDoneOnItem={this.handleToggleDoneOnItem}
                            deleteItem={this.handledeleteItem}
                            className={classes.smallMarginBotton}
                        />
                        {shoppingList.items.length > 0 && (
                            <div className={classes.mdPaddingSides}>
                                <Button
                                    variant="outlined"
                                    className={classes.button}
                                    type="submit"
                                    fullWidth
                                    onClick={this.copyItemsToClipboard}
                                >
                                    <FileCopyOutlinedIcon />
                                </Button>
                            </div>
                        )}
                    </React.Fragment>
                )}
                <Fab
                    variant="extended"
                    className={classes.fab}
                    onClick={this.handleClickRename}
                >
                    <EditIcon />
                    Renomear
                </Fab>
                <TextFormModal
                    open={this.state.editNameModalIsOpen}
                    handleClose={this.handleCloseEditNameModal}
                    initialText={shoppingList.name}
                    onConfirm={this.onConfirmEditName}
                />
            </div>
        )
    }
}

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
    },
    fab: {
        position: 'fixed',
        bottom: '50px',
        right: '20px',
    },
    mdPaddingSides: {
        padding: '0 20px',
    },
    centerChild: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default compose(
    withStyles(styles),
    withRouter,
    withShoppingListContext,
    withShoppingListControlContext,
    withShoppingListItemContext,
    withShoppingListItemControlContext
)(ShoppingList)
