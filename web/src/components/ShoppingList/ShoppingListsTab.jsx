import { CircularProgress } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import Fab from '@material-ui/core/Fab'
import { withStyles } from '@material-ui/core/styles'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import EditIcon from '@material-ui/icons/Edit'
import { compose } from 'lodash/fp'
import React from 'react'
import { withRouter } from 'react-router'
import 'react-toastify/dist/ReactToastify.css'
import ShoppingListCard from 'src/components/ShoppingList/ShoppingListCard'
import { createShoppingList } from 'src/requests'
import {
    withShoppingListContext,
    withShoppingListControlContext,
} from './ShoppingListContext/ShoppingListContext'

class ShoppingListsTab extends React.Component {
    handleClickAdd = async () => {
        const {
            history,
            shoppingListControl: { addShoppingList },
        } = this.props
        const { data: newShoppingList } = await createShoppingList()
        addShoppingList(newShoppingList)
        console.log('criou shoppinglist', newShoppingList)
        history.push(`/lista-de-compras/${newShoppingList._id}`)
    }

    handleClickEditItems = () => {
        const { history } = this.props
        history.push(`/itens-compras`)
    }

    render() {
        const {
            classes,
            shoppingListContext: { shoppingLists, loading },
        } = this.props
        return (
            <div className={classes.root}>
                <Card
                    raised
                    className={classes.addCard}
                    onClick={this.handleClickAdd}
                >
                    <AddCircleOutlineIcon
                        style={{ opacity: 0.2, fontSize: 80 }}
                    />
                </Card>
                {loading && <CircularProgress />}
                {!loading &&
                    shoppingLists.map((shoppingList) => {
                        return (
                            <ShoppingListCard
                                key={shoppingList._id}
                                shoppingList={shoppingList}
                            />
                        )
                    })}
                <Fab
                    variant="extended"
                    className={classes.fab}
                    onClick={this.handleClickEditItems}
                >
                    <EditIcon />
                    Editar itens
                </Fab>
            </div>
        )
    }
}

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
    },
    addCard: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '100px',
        cursor: 'pointer',
    },
    centerChild: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    fab: {
        position: 'fixed',
        bottom: '50px',
        right: '20px',
    },
})

export default compose(
    withStyles(styles),
    withRouter,
    withShoppingListControlContext,
    withShoppingListContext
)(ShoppingListsTab)
