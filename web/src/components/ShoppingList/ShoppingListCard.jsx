import { IconButton, Typography } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import { compose } from 'lodash/fp'
import React from 'react'
import { withRouter } from 'react-router'
import 'react-toastify/dist/ReactToastify.css'
import { getDate_ddmmyyyy } from 'src/utils/getDate_ddmmyyyy'
import {
    withShoppingListContext,
    withShoppingListControlContext,
} from './ShoppingListContext/ShoppingListContext'

class ShoppingListCard extends React.Component {
    handleClick = () => {
        const { shoppingList, history } = this.props

        history.push(`/lista-de-compras/${shoppingList._id}`)
    }
    
    handleClickDelete = async (event) => {
        event.stopPropagation()
        const {
            shoppingListControl: { handleDeleteShoppingList },
            shoppingList,
        } = this.props
        await handleDeleteShoppingList(shoppingList._id)
    }

    render() {
        const { classes, shoppingList } = this.props
        console.log(shoppingList)
        return (
            <div className={classes.root}>
                <Card
                    raised
                    className={classes.card}
                    onClick={this.handleClick}
                >
                    <Grid container spacing={3}>
                        <Grid item xs={1} />
                        <Grid item xs={6} className={classes.centerVertically}>
                            <Typography> {shoppingList.name}</Typography>
                        </Grid>
                        <Grid item xs={2} />
                        <Grid
                            item
                            xs={2}
                            className={classes.centerVerticallyEndHorizontally}
                        >
                            <IconButton onClick={this.handleClickDelete}>
                                <CloseIcon />
                            </IconButton>
                        </Grid>
                        <Grid item xs={1} />

                        <Grid item xs={1} />
                        <Grid item xs={3} className={classes.centerVertically}>
                            <Typography>
                                {`${shoppingList.items.length} itens`}
                            </Typography>
                        </Grid>

                        <Grid
                            item
                            xs={7}
                            className={classes.centerVerticallyEndHorizontally}
                        >
                            <Typography
                                variant={'body2'}
                                className={classes.fancy}
                            >
                                {`Criada em ${getDate_ddmmyyyy(
                                    shoppingList.createdAt
                                )}`}
                            </Typography>
                        </Grid>
                        <Grid item xs={1} />
                    </Grid>
                </Card>
            </div>
        )
    }
}

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
        minHeight: '100px',
    },
    card: {
        width: '100%',
        cursor: 'pointer',
    },
    fancy: {
        marginLeft: '4px',
        fontFamily: 'Norican, cursive',
    },
    centerVertically: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        minHeight: '50px',
    },
    centerVerticallyEndHorizontally: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        minHeight: '50px',
    },
    someMarginBottom: {},
})

export default compose(
    withStyles(styles),
    withRouter,
    withShoppingListControlContext,
    withShoppingListContext
)(ShoppingListCard)
