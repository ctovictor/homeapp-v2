import AppBar from '@material-ui/core/AppBar'
import Container from '@material-ui/core/Container'
import { withStyles } from '@material-ui/core/styles'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import React from 'react'
import { withRouter } from 'react-router'
import { Link, Redirect, Route, Switch } from 'react-router-dom'
import ShoppingListProvider from 'src/components/ShoppingList/ShoppingListContext/ShoppingListProvider'
import ShoppingListItemProvider from 'src/components/ShoppingListItem/ShoppingListItemContext/ShoppingListItemProvider'
import ShoppingListItemsTab from 'src/components/ShoppingListItem/ShoppingListItemsTab'
import withAuthContext from 'src/context/withAuthContext'
import Login from './Login'
import NoticesTab from './NoticesTab'
import ShoppingList from './ShoppingList/ShoppingList'
import ShoppingListsTab from './ShoppingList/ShoppingListsTab'

class AppContent extends React.Component {
    state = {
        currentTab: 0,
    }
    componentDidMount() {
        this.setState({
            currentTab: this.getTabFromPathname(this.props.location.pathname),
        })
    }
    handleChange = (event, newValue) => {
        this.setState({ currentTab: newValue })
    }
    getTabFromPathname = (pathname) => {
        switch (pathname) {
            case '/listas-de-compras':
            case '/itens-compras':
                return 1
            case '/receitas':
                return 2
            default:
                return 0
        }
    }
    render() {
        const isLoginScreen = this.props.location.pathname === '/login'

        const { currentTab } = this.state
        const { classes } = this.props

        return (
            <div className={classes.content}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={currentTab}
                        aria-label="tabs"
                        onChange={this.handleChange}
                        variant="fullWidth"
                        textColor="primary"
                        indicatorColor="primary"
                    >
                        <Tab label="Avisos" component={Link} to="/avisos" />
                        <Tab
                            label="Lista de Compras"
                            component={Link}
                            to="/listas-de-compras"
                        />
                        <Tab
                            label="Receitas"
                            component={Link}
                            to="/receitas"
                            disabled
                        />
                    </Tabs>
                </AppBar>
                <Container maxWidth="md">
                    <Switch>
                        <Route exact path="/" component={NoticesTab} />
                        <Route path="/avisos" component={NoticesTab} />
                        <Route
                            path="/listas-de-compras"
                            component={ShoppingListsTab}
                        />
                        <Route
                            path="/itens-compras"
                            component={ShoppingListItemsTab}
                        />
                        <Route
                            path="/lista-de-compras/:shoppingListId"
                            component={ShoppingList}
                        />
                        <Route path="/receitas" component={NoticesTab} />

                        <Redirect to="/" />
                    </Switch>
                </Container>
            </div>
        )
    }
}

const styles = (theme) => ({
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    content: {
        flex: '1 0 auto',
        paddingBottom: '10px',
    },
})

const WrappedComponent = withAuthContext(
    withRouter(withStyles(styles)(AppContent))
)

export default (props) => (
    <ShoppingListProvider>
        <ShoppingListItemProvider>
            <WrappedComponent {...props} />
        </ShoppingListItemProvider>
    </ShoppingListProvider>
)
