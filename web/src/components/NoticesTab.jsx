import { withStyles } from '@material-ui/core/styles'
import React from 'react'
import {
    deleteNotice,
    getAllNotices,
    modifyOneNotice,
    postNotice,
} from 'src/requests'
import NoticesList from './NoticesList'
import TextForm from './TextForm'

const styles = theme => ({
    root: {
        paddingTop: '10px',
    },
})

class NoticesTab extends React.Component {
    state = {
        loading: true,
        notices: [],
    }
    componentDidMount() {
        this.fetchFromDatabase()
    }
    addNotice = async text => {
        const notice = { text, done: false }
        try {
            const res = await postNotice(notice)
            if (res.status < '400') {
                this.fetchFromDatabase()
            }
            console.log(res)
        } catch (e) {
            console.warn(e)
        }
    }
    fetchFromDatabase = async () => {
        try {
            const res = await getAllNotices()
            const notices = res.data.map(notice => ({
                text: notice.text,
                _id: notice._id,
                date: notice.date,
                done: notice.done,
            }))
            this.setState({ notices, loading: false })
        } catch (e) {
            console.warn(e)
        }
    }
    toggleDoneOnItem = index => {
        const { notices } = this.state
        notices[index].done = !notices[index].done
        modifyOneNotice(notices[index]._id, { done: notices[index].done })
        this.setState({ notices })
    }
    deleteItem = async id => {
        try {
            const res = await deleteNotice(id)
            this.fetchFromDatabase()
        } catch (e) {
            console.warn(e)
        }
    }
    render() {
        const { classes } = this.props

        return (
            <div className={classes.root}>
                <TextForm addItem={this.addNotice} item={'aviso'} />
                <NoticesList
                    notices={this.state.notices}
                    toggleDoneOnItem={this.toggleDoneOnItem}
                    deleteItem={this.deleteItem}
                />
            </div>
        )
    }
}

export default withStyles(styles)(NoticesTab)
