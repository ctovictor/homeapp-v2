import { TextField } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { compose } from 'lodash/fp'
import React from 'react'
import { withRouter } from 'react-router'
import 'react-toastify/dist/ReactToastify.css'
import {
    withShoppingListContext,
    withShoppingListControlContext,
} from '../components/ShoppingList/ShoppingListContext/ShoppingListContext'

class TextFormModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            textTyped: props.initialText,
        }
    }
    handleInputChange = (event) => {
        this.setState({ textTyped: event.target.value })
    }

    handleSubmit = (event) => {
        if (event) event.preventDefault()
        const { onConfirm } = this.props
        onConfirm(this.state.textTyped)
    }
    render() {
        const { open, handleClose, classes } = this.props
        const { textTyped } = this.state

        return (
            <Dialog
                fullScreen={true}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {'Renomear Lista de compras'}
                </DialogTitle>
                <DialogContent>
                    <form onSubmit={this.handleSubmit} className={classes.root}>
                        <Grid container className={classes.root} spacing={2}>
                            <Grid item xs={12} className={classes.center}>
                                <TextField
                                    fullWidth
                                    value={textTyped}
                                    autoFocus
                                    placeholder={`Digite um novo nome`}
                                    onChange={this.handleInputChange}
                                ></TextField>
                            </Grid>
                            <Grid item xs={6}></Grid>
                            <Grid item xs={6} className={classes.center}></Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Cancelar
                    </Button>
                    <Button
                        autoFocus
                        onClick={this.handleSubmit}
                        color="primary"
                    >
                        Confirmar
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

const styles = (theme) => ({
    root: {
        flexGrow: 1,
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
    },
})

export default compose(
    withStyles(styles),
    withRouter,
    withShoppingListContext,
    withShoppingListControlContext
)(TextFormModal)
