import { CircularProgress } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined'
import { compose } from 'lodash/fp'
import React from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import TextForm from 'src/components/TextForm'
import {
    getAllShoppingListItems,
    modifyOneShoppingListItem,
    postShoppingListItem,
} from 'src/requests'
import { withShoppingListItemControlContext } from '../ShoppingListItem/ShoppingListItemContext/ShoppingListItemContext'
import ShoppingListItemsList from './ShoppingListItemsList'
class ShoppingListItemsTab extends React.Component {
    state = {
        loading: true,
        items: [],
    }
    componentDidMount() {
        this.fetchFromDatabase()
    }
    addItem = async (text) => {
        const item = { text, done: false }
        try {
            const res = await postShoppingListItem(item)
            if (res.status < '400') {
                this.fetchFromDatabase()
            }
            console.log(res)
        } catch (e) {
            console.warn(e)
        }
    }
    fetchFromDatabase = async () => {
        try {
            const res = await getAllShoppingListItems({
                sortParams: { done: 1, text: 1 },
            })
            const items = res.data.map((item) => ({
                text: item.text,
                _id: item._id,
                date: item.date,
                done: item.done,
            }))
            this.setState({ items, loading: false })
        } catch (e) {
            console.warn(e)
        }
    }
    toggleDoneOnItem = (index) => {
        const { items } = this.state
        items[index].done = !items[index].done
        //modifyOneShoppingListItem(items[index]._id, { done: items[index].done })
        this.setState({ items })
    }
    deleteItem = async (id) => {
        try {
            const {
                shoppingListItemControl: { deleteShoppingListItem },
            } = this.props
            const res = await deleteShoppingListItem(id)
            console.log(res)
            this.fetchFromDatabase()
        } catch (e) {
            console.warn(e)
        }
    }
    copyItemsToClipboard = () => {
        const el = document.createElement('textarea')
        const items = this.state.items.filter((item) => !item.done)
        const text = items.reduce(
            (acumulado, item) => `${acumulado}${item.text}\n`,
            ''
        )
        console.log(text)
        el.value = text
        document.body.appendChild(el)
        el.select()
        document.execCommand('copy')
        document.body.removeChild(el)
        toast('Itens copiados para a área de transferência!', {
            autoClose: 4000,
        })
    }
    render() {
        const { classes } = this.props
        const { loading } = this.state
        return (
            <div className={classes.root}>
                <TextForm addItem={this.addItem} item={'item'} />
                {loading && (
                    <div className={classes.centerChild}>
                        <CircularProgress />
                    </div>
                )}
                {!loading && (
                    <React.Fragment>
                        <ShoppingListItemsList
                            items={this.state.items}
                            toggleDoneOnItem={this.toggleDoneOnItem}
                            deleteItem={this.deleteItem}
                            className={classes.smallMarginBotton}
                        />
                        {this.state.items.length > 0 && (
                            <div className={classes.mdPaddingSides}>
                                <Button
                                    variant="outlined"
                                    className={classes.button}
                                    type="submit"
                                    fullWidth
                                    onClick={this.copyItemsToClipboard}
                                >
                                    <FileCopyOutlinedIcon></FileCopyOutlinedIcon>
                                </Button>
                            </div>
                        )}
                    </React.Fragment>
                )}
            </div>
        )
    }
}

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
    },
    mdPaddingSides: {
        padding: '0 20px',
    },
    centerChild: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default compose(
    withStyles(styles),
    withShoppingListItemControlContext
)(ShoppingListItemsTab)
