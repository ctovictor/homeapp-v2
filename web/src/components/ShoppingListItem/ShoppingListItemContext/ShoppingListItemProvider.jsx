import React from 'react'
import {
    ShoppingListItemContext,
    ShoppingListItemControlContext,
} from './ShoppingListItemContext'
import {
    getAllShoppingListItems,
    postShoppingListItem,
    deleteShoppingListItem,
} from 'src/requests'

class ShoppingListItemProvider extends React.Component {
    state = {
        shoppingListItems: [],
        loading: true,
    }
    constructor(props) {
        super(props)
        this.shoppingListItemControl = {
            mapOptionsToShoppingListItems: this.mapOptionsToShoppingListItems,
            mapOptionToShoppingListItem: this.mapOptionToShoppingListItem,
            mapShoppingListItemsToOptions: this.mapShoppingListItemsToOptions,
            createShoppingListItem: this.createShoppingListItem,
            deleteShoppingListItem: this.deleteShoppingListItem,
        }
    }
    async componentDidMount() {
        const shoppingListItems = await this.fetchFromDatabase()
        this.setState({ shoppingListItems, loading: false })
    }
    fetchFromDatabase = async () => {
        try {
            const { data: shoppingListItems } = await getAllShoppingListItems({
                sortParams: { done: 1, text: 1 },
            })
            return shoppingListItems
        } catch (e) {
            console.error(e)
        }
    }
    createShoppingListItem = async (text) => {
        const { data: newItem } = await postShoppingListItem({ text })
        this.setState({
            shoppingListItems: [...this.state.shoppingListItems, newItem],
        })
        return newItem
    }
    deleteShoppingListItem = async (id) => {
        const res = await deleteShoppingListItem(id)
        this.setState({
            shoppingListItems: this.state.shoppingListItems.filter(
                (item) => item._id !== id
            ),
        })
    }

    mapOptionsToShoppingListItems = (options) => {
        if (!options) {
            options = []
        }
        return options.map((option) => {
            return this.mapOptionToShoppingListItem(option)
        })
    }

    mapOptionToShoppingListItem = (option) => {
        return {
            ...option,
            value: undefined,
            label: undefined,
        }
    }

    mapShoppingListItemsToOptions = (shoppingListItems) => {
        if (!shoppingListItems) {
            shoppingListItems = []
        }
        return shoppingListItems.map((shoppingListItem) => {
            return this.mapShoppingListItemToOption(shoppingListItem)
        })
    }

    mapShoppingListItemToOption = (shoppingListItem) => {
        return {
            ...shoppingListItem,
            label: shoppingListItem.text,
            value: shoppingListItem.text,
        }
    }

    render() {
        return (
            <ShoppingListItemContext.Provider value={this.state}>
                <ShoppingListItemControlContext.Provider
                    value={this.shoppingListItemControl}
                >
                    {this.props.children}
                </ShoppingListItemControlContext.Provider>
            </ShoppingListItemContext.Provider>
        )
    }
}

export default ShoppingListItemProvider
