import React from 'react'

export const ShoppingListItemContext = React.createContext()
ShoppingListItemContext.displayName = 'ShoppingListItemContext'
export const withShoppingListItemContext = (Component) => (props) => (
    <ShoppingListItemContext.Consumer>
        {(state) => (
            <Component {...props} shoppingListItemContext={state}></Component>
        )}
    </ShoppingListItemContext.Consumer>
)

export const ShoppingListItemControlContext = React.createContext()
ShoppingListItemControlContext.displayName = 'ShoppingListItemControlContext'
export const withShoppingListItemControlContext = (Component) => (props) => (
    <ShoppingListItemControlContext.Consumer>
        {(state) => (
            <Component {...props} shoppingListItemControl={state}></Component>
        )}
    </ShoppingListItemControlContext.Consumer>
)
