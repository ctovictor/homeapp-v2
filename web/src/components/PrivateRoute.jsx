import { compose } from 'lodash/fp'
import React from 'react'
import { Redirect, withRouter } from 'react-router'
import { Route } from 'react-router-dom'
import withAuthContext from 'src/context/withAuthContext'

class PrivateRoute extends React.Component {
    render() {
        const {
            context: {
                authActions: { isAuthenticated },
            }, } = this.props
        const loginUrl = '/login'
        const loggedIn = isAuthenticated()
        if (!loggedIn) {
            return <Redirect to={loginUrl} />
        }
        return <Route {...this.props} />
    }
}

export default compose(withAuthContext, withRouter)(PrivateRoute)
