import CssBaseline from '@material-ui/core/CssBaseline'
import {
    createMuiTheme,
    ThemeProvider,
    withStyles
} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { compose } from 'lodash/fp'
import React from 'react'
import { withRouter } from 'react-router'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import withAuthContext from 'src/context/withAuthContext'
import AppContent from './AppContent'
import Login from './Login'
import PrivateRoute from './PrivateRoute'

//TODO not overriding.
const theme = createMuiTheme({
    overrides: {
        MuiCssBaseline: {
            '@global': {
                body: {
                    backgroundColor: '#a26262',
                },
            },
        },
    },
})

class App extends React.Component {
    isLoginScreen = () => {
        return this.props.location.pathname === '/login'
    }

    render() {
        const {
            classes,
            context: {
                authActions: { isAuthenticated },
            },
        } = this.props
        // console.log({
        //     isAuthenticated: isAuthenticated(),
        //     isLoginScreen: this.isLoginScreen(),
        //     location: this.props.location.pathname,
        // })
        return (
            <Router>
                <CssBaseline />
                <ThemeProvider theme={theme}>
                    <div className={classes.root}>
                        <Switch>
                            <Route exact path="/login" component={Login} />
                            <PrivateRoute path="/*" component={AppContent} />
                        </Switch>

                        <footer className={classes.footer}>
                            <Typography>Casa Cortez</Typography>
                        </footer>
                    </div>
                    <ToastContainer />
                </ThemeProvider>
            </Router>
        )
    }
}

const styles = (theme) => ({
    root: {
        height: '100vh',
        flexDirection: 'column',
        display: 'flex',
    },
    fab: {
        margin: theme.spacing(1),
        top: 'auto',
        left: 20,
        bottom: 20,
        right: 'auto',
        position: 'fixed',
        // backgroundColor: "MediumTurquoise",
        // color: "white"
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
        // color: "white"
    },
    footer: {
        flexShrink: 0,
        backgroundColor: '#099e99',
        display: 'flex',
        flexDirection: 'row',
        color: 'white',
        justifyContent: 'center',
        padding: '5px',
        // '& div': {
        //     '&:nth-child(1)': {
        //         width: 'calc(100% / 3 * 2)',
        //     },
        //     '&:nth-child(2)': {
        //         width: 'calc(100% / 3 * 1)',
        //         textAlign: 'center',
        //         fontSize: '200%',
        //         color: 'white',
        //     },
        // },
    },
    content: {
        flex: '1 0 auto',
        paddingBottom: '10px',
    },
})

export default compose(withStyles(styles), withRouter, withAuthContext)(App)
