import { TextField } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/styles'
import React from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

const styles = {
    root: {
        flexGrow: 1,
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
    },
}

class TextFormModal extends React.Component {
    state = {
        textTyped: '',
    }

    handleInputChange = event => {
        this.setState({ textTyped: event.target.value })
    }

    handleSubmit = event => {
        event.preventDefault()
        this.props.addItem(this.state.textTyped)
        this.setState({ textTyped: '' })
        //TODO focus the text field again.
    }

    render() {
        const { textTyped } = this.state
        const { classes, item } = this.props

        return (
            <React.Fragment>
                <form onSubmit={this.handleSubmit} className={classes.root}>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} className={classes.center}>
                            <TextField
                                fullWidth
                                value={textTyped}
                                autoFocus
                                placeholder={`Digite um novo ${item}`}
                                onChange={this.handleInputChange}
                            ></TextField>
                        </Grid>
                        <Grid item xs={6}></Grid>
                        <Grid item xs={6} className={classes.center}>
                            <Button
                                variant="outlined"
                                className={classes.button}
                                disabled={!!!textTyped}
                                type="submit"
                            >
                                <Typography>{`criar ${item}`}</Typography>
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(TextFormModal)
