import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import withAuthContext from 'src/context/withAuthContext'
import Container from '@material-ui/core/Container'
import { toast } from 'react-toastify'

const styles = (theme) => ({
    root: {
        paddingTop: '10px',
        flex: '1 0 auto',
    },
    smallMargin: {
        margin: 15,
    },
})

class Login extends React.Component {
    state = {
        loginInfo: null,
        username: '',
        password: '',
    }

    handleChangeUsername = (event) => {
        event.preventDefault()
        this.setState({
            username: event.target.value,
        })
    }
    handleChangePassword = (event) => {
        event.preventDefault()
        this.setState({
            password: event.target.value,
        })
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        const {
            context: { authActions },
            history,
        } = this.props
        const success = await authActions.login(this.state.username, this.state.password)
        if (success) {
            history.push('/')
        } else {
            toast.error('Login falhou', {
                autoClose: 4000,
            })
        }
    }

    render() {
        const { classes } = this.props
        const {
            authState: { isAuthenticated },
        } = this.props.context

        return (
            <div className={classes.root}>
                <Container maxWidth="md">
                    <Typography component="h1" variant="h5">
                        Casa Cortez
                    </Typography>
                    <form className={classes.form} onSubmit={this.handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Usuario"
                            name="name"
                            autoComplete="name"
                            autoFocus
                            onChange={this.handleChangeUsername}
                        />

                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Senha"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={this.handleChangePassword}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Entrar
                        </Button>
                    </form>
                </Container>
            </div>
        )
    }
}

export default withAuthContext(withStyles(styles)(Login))
